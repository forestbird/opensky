from functools import partial
from typing import Optional, Tuple, Iterable, NewType, Union

import click
import attr
import requests
from haversine import haversine

OPENSKY_API_URL = 'https://opensky-network.org/api'


Kilometers = NewType('Kilometers', Union[int, float])


@attr.s(auto_attribs=True, frozen=True, slots=True)
class City:
    name: str
    latitude: float
    longitude: float

    @property
    def coordinates(self) -> Tuple[float, float]:
        return self.latitude, self.longitude


@attr.s(auto_attribs=True, frozen=True, slots=True)
class Aircraft:
    icao24: Optional[str]
    callsign: str = attr.ib(converter=str.strip)
    longitude: Optional[float]
    latitude: Optional[float]

    @property
    def coordinates(self) -> Tuple[float, float]:
        return self.latitude, self.longitude

    @property
    def position_is_known(self) -> bool:
        return self.latitude is not None and self.longitude is not None


def fetch_all_aircrafts() -> Iterable[Aircraft]:
    r = requests.get(OPENSKY_API_URL + '/states/all')

    return (Aircraft(icao24=i[0], callsign=i[1],
                     longitude=i[5], latitude=i[6])
            for i in r.json()['states'])


def is_near(point_a: Tuple[float, float],
            distance: Kilometers, point_b: Tuple[float, float]) -> bool:
    return haversine(point_a, point_b) < distance


def fetch_aricrafts_around_city(city: City, distance: Kilometers) -> Iterable[Aircraft]:
    is_within_distance = partial(is_near, city.coordinates, distance)

    for aircraft in fetch_all_aircrafts():
        if aircraft.position_is_known and is_within_distance(aircraft.coordinates):
            yield aircraft


def fetch_aricrafts_within_450_km_of_paris() -> Iterable[Aircraft]:
    paris = City('Paris', 48.864716, 2.349014)

    return fetch_aricrafts_around_city(paris, distance=Kilometers(450))


@click.command()
def main():
    for i in fetch_aricrafts_within_450_km_of_paris():
        print(i)


if __name__ == '__main__':
    main()
