import re
from setuptools import setup, find_packages
from pathlib import Path

HERE = Path(__file__).parent.resolve()

__INIT_PY__ = HERE.joinpath('src/sky_of_paris/__init__.py')


def find_version():
    with open(__INIT_PY__) as f:
        version_match = re.search(
            r"^__version__ = ['\"]([^'\"]*)['\"]", f.read(), re.MULTILINE
        )
        if version_match:
            return version_match.group(1)
        else:
            raise RuntimeError(f'Unable to find __version__ string in {__INIT_PY__}!')


install_requires = [
    'requests==2.18.4',
    'haversine==0.4.5',
    'click==6.7.0',
    'attrs==17.4.0',
]

setup(
    name='sky_of_paris',
    version=find_version(),
    description='',
    classifiers=[
        "Programming Language :: Python :: 3.6"
    ],
    author='',
    author_email='',
    url='',
    packages=find_packages(where='src'),
    package_dir={"": "src"},
    include_package_data=True,
    install_requires=install_requires,
    python_requires='>=3.6',
    entry_points="""
        [console_scripts]
        scan_sky_of_paris=sky_of_paris.scanner:main
    """
)
