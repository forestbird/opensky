import unittest

import requests

from sky_of_paris import fetch_aricrafts_within_450_km_of_paris, Aircraft

FAKE_STATES = [
    ['4240eb', 'UTA358  ', 'United Kingdom', 1520015569, 1520015569, 38.2509, 54.3446, 7886.7,
     False, 232.97, 289.08, -4.88, None, 7879.08, '4174', False, 0],
    ['a8aac8', 'DAL530  ', 'United States', 1520015569, 1520015569, -83.3853, 28.4134, 10721.34,
     False, 229.68, 4.63, 5.2, None, 11148.06, '2725', False, 0],
    ['aa56db', 'UAL2571 ', 'United States', 1520015569, 1520015569, -117.0253, 33.7083, 3398.52,
     False, 179.12, 96.43, 3.9, None, 3421.38, '7237', False, 0],
    ['ac52ee', 'ASH6029 ', 'United States', 1520015557, 1520015567, -92.264, 34.2034, 10668, False,
     268.74, 65.94, -0.33, None, 10911.84, '3456', False, 0],
    ['a0cfbd', 'AAL690  ', 'United States', 1520015566, 1520015567, -75.3739, 43.1119, 8534.4,
     False, 244.16, 274.71, 0, None, 8412.48, '3515', False, 0],
    ['a52911', 'RPA4683 ', 'United States', 1520015564, 1520015566, -78.8023, 42.8847, 571.5, False,
     75.68, 43.9, -3.9, None, 579.12, '3044', False, 0],
    ['ab6fdd', 'AAL978  ', 'United States', 1520015552, 1520015553, -63.4425, 19.176, 10363.2,
     False, 221.42, 331.71, 0, None, 10850.88, '1455', False, 0],
    ['c0669e', 'WJA546  ', 'Canada', 1520015569, 1520015569, -81.2774, 45.0195, 10408.92, False,
     240.24, 137.08, -12.68, None, 10195.56, '7120', False, 0],
    ['8a02fe', 'GIA888  ', 'Indonesia', 1520015569, 1520015569, 113.7524, 2.7201, 10668, False,
     242.58, 22.7, -0.33, None, 11346.18, '2447', False, 0],
    ['a92d6d', 'UAL557  ', 'United States', 1520015569, 1520015569, -77.0564, 38.3722, 11887.2,
     False, 257.35, 36.14, 0, None, 11818.62, '2473', False, 0],
    ['479f4f', 'NAX45F  ', 'Norway', 1520015569, 1520015569, 20.8423, 63.7758, 10668, False, 236.09,
     22.82, 0, None, 10248.9, '0220', False, 0],
    ['340003', '', 'Spain', None, 1520015404, None, None, None, False, 3.13, 99.46, -0.33, None,
     None, None, False, 0],
    ['ade8fa', 'AAL1614 ', 'United States', 1520015569, 1520015569, -80.0767, 25.8461, 960.12,
     False, 142.82, 187.87, -9.1, None, 1043.94, '1657', False, 0],
    ['4d0103', 'CLX752  ', 'Luxembourg', 1520015567, 1520015569, 56.9082, 25.012, 11887.2, False,
     290.47, 101.65, 0, None, 12260.58, '2624', False, 0],
    ['a2cbbd', 'N28BB   ', 'United States', 1520015569, 1520015569, -118.3064, 34.1343, 1394.46,
     False, 55.23, 205.37, -3.58, None, 1417.32, '4751', False, 0],
    ['a5f845', 'N484JH  ', 'United States', 1520015569, 1520015569, -116.8413, 33.3659, 5120.64,
     False, 158.6, 65.26, 0.33, None, 5113.02, '4667', False, 0],
    ['ad6bdc', 'JBU615  ', 'United States', 1520015569, 1520015569, -79.8288, 40.1236, 10378.44,
     False, 215.04, 270.27, 0, None, 10233.66, '2730', False, 0],
    ['e4940e', 'PRDVT   ', 'Brazil', 1520015569, 1520015569, -55.9137, -10.3653, 11887.2, False,
     190.54, 173.49, 0, None, 12565.38, None, False, 0],
    ['aa5b92', 'AAL225  ', 'United States', 1520015568, 1520015569, -93.2742, 29.9005, 10058.4,
     False, 287.72, 103.86, 0, None, 10492.74, '0201', False, 0],
    ['a01333', 'MRA755  ', 'United States', 1520015496, 1520015497, -96.2448, 33.0645, 365.76,
     False, 66.56, 175.57, -7.48, None, 426.72, '1200', False, 0],
    ['4d00f3', 'LGL7788 ', 'Luxembourg', 1520015569, 1520015569, 8.0073, 49.7915, 8534.4, False,
     199.72, 271.92, -0.65, None, 8343.9, '4165', False, 0],
    ['4caa57', 'RYR7BV  ', 'Ireland', 1520015569, 1520015569, -0.9959, 51.5329, 8839.2, False,
     217.25, 156.41, 0, None, 8534.4, '7712', False, 0]
]


class TestFetchAircraftsAroundParis(unittest.TestCase):
    def setUp(self):
        def fake_get(*args, **kwargs):
            class FakeResonse:
                def json(self):
                    return {'time': 1520015570, 'states': FAKE_STATES}
            return FakeResonse()

        self.real_requests_get = requests.get
        requests.get = fake_get

    def tearDown(self):
        requests.get = self.real_requests_get

    def test(self):
        """Simple functional test to make sure all kind of sort of works."""
        aircrafts = list(fetch_aricrafts_within_450_km_of_paris())

        expected = [
            Aircraft(icao24='4d00f3', callsign='LGL7788', longitude=8.0073, latitude=49.7915),
            Aircraft(icao24='4caa57', callsign='RYR7BV', longitude=-0.9959, latitude=51.5329),
        ]

        self.assertEqual(aircrafts, expected)


if __name__ == '__main__':
    unittest.main()
